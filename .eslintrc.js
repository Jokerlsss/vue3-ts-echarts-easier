// eslint-disable-next-line no-undef
module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: ["eslint:recommended", "plugin:vue/vue3-essential"],
    parserOptions: {
        ecmaVersion: "latest",
        parser: "@typescript-eslint/parser",
        sourceType: "module",
    },
    plugins: ["vue", "@typescript-eslint"],
    rules: {
        "no-unused-vars": ["off"],
        quotes: [2, "double"],
        indent: ["warn", 4, {ignoredNodes: ["TemplateLiteral"], SwitchCase: 1}],
        "vue/multi-word-component-names": "off", //关闭文件命名规则校验
        "array-bracket-spacing": [2, "never"],
        "comma-spacing": [2, {
            "before": false,
            "after": true
        }],
        "no-irregular-whitespace": 2,
        "no-multi-spaces": 1,
        "no-trailing-spaces": 1,
        "space-before-blocks": [0, "always"],
        "linebreak-style": [0, "windows"],
    },
}
