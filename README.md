# 可视化 ECharts 代码生成器

## 解决痛点

1. **ECharts 文档属性太多**，有点“乱花渐欲迷人眼”的感觉？
2. 只是想做个简单图表，却在文档中迷失自我，效率低下？

&emsp;&emsp;因此，可视化生成 ECharts 代码的此应用诞生！把最经常用到的 ECharts 图表及其常用属性，用可视化操作来代替，满足**日常轻量开发**场景，无需只为改一个柱状颜色而在文档中寻寻觅觅~欢迎 ❤Star

## 如何使用

- 安装依赖：`npm install`
- 运行：`npm run dev`

## 笔者的话

- 【项目简单】该项目目前能实现的功能比较简单，就笔者个人而言，**柱状、折线、饼图** 几乎涵盖了项目开发中的 90%
- 【共同参与】但，既然作为一款希望帮助每个开发者提效的工具，我希望有能力的你 **一起参与进来**，添加更多图表类型和属性，为更多人服务
- 【保持初心】需要明确的一点是，这款软件不应将不常用的属性或图表添加进来，因为我希望它是一款 **上手简单、使用简洁易懂** 的软件，如果内容太过于全面，其使用成本不亚于去看 ECharts 官方文档，这是我所不希望的！用 20%的内容解决 80%的场景即可，这不是一本字典，而是趁手的工具
- 【持续反馈】最后，如果你觉得这个 **项目有不足之处，请留下宝贵的意见**；如果你觉得能帮到你，还请用 **两秒钟的时间**，留下你的 **Star**，这将是笔者不断进步的动力，感谢 ❤
