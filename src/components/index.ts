import EChartsConfig from './EChartsDesign/EChartsConfig.vue'
import EChartsPreview from './EChartsDesign/EChartsPreview.vue'
import EChartsSelect from './EChartsSelect/EChartsSelect.vue'
// 全局注册组件
export default function (app: any): void {
  app.component('LEChartsConfig', EChartsConfig)
  app.component('LEChartsPreview', EChartsPreview)
  app.component('LEChartsSelect', EChartsSelect)
}
