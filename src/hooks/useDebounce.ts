let timer: any = null

export function useDebounce(fn: Function, delay = 500) {
    if (timer) {
        clearTimeout(timer)
    }
    timer = setTimeout(() => {
        fn.apply(null, arguments)
        timer = null
    }, delay)
}
