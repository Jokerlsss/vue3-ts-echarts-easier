import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import { createPinia } from "pinia"
import ElementPlus from "element-plus"
import "element-plus/dist/index.css"
import components from "./components/index"
import "./assets/svg/iconfont.css"
import "./assets/svg/iconfont.js"
import * as ElementPlusIconsVue from "@element-plus/icons-vue"

// 创建实例
const app = createApp(App)
const pinia = createPinia()

// ElementPlus icons注册
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

// 全局注册组件
components(app)

app.use(router).use(pinia).use(ElementPlus).mount("#app")
