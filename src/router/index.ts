import {
    createRouter,
    createWebHashHistory,
    Router,
    RouteRecordRaw,
    RouterOptions,
} from "vue-router"

const routes: RouteRecordRaw[] = [
    {
    // index
        path: "/",
        name: "Index",
        component: () => import("../views/Index.vue"),
    },
]

const options: RouterOptions = {
    history: createWebHashHistory(),
    routes,
}

const router: Router = createRouter(options)
export default router
