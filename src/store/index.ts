import { defineStore, StoreDefinition } from "pinia"
import { chartDefaultConfig } from "../types/EChartsTypes"

export const chartStore: StoreDefinition = defineStore("chart", {
    state() {
        return {
            // 图表源代码
            originCode: "",
            // 当前编辑的图表类型
            curEditChartType: "",
            // 图表配置数据
            form: chartDefaultConfig().BASE(),
        }
    },
    actions: {
        setOriginCode(value) {
            this.originCode = value
        },
        setCurEditChartType(value) {
            this.curEditChartType = value
        },
        // 重置图表配置项
        resetForm(newForm) {
            // 1.清空原对象所有属性
            Object.keys(this.form).forEach((key) => {
                Reflect.deleteProperty(this.form, key)
            })
            // 2.将新属性合并到原对象中
            Object.assign(this.form, newForm)
        },
    },
})
