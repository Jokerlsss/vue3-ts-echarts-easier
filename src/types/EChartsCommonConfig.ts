// 图表标题
export interface Title {
  text?: string
  subtext?: string
  left?: Title_Left
}

// 标题位置
export enum Title_Left {
  LEFT = "left",
  CENTER = "center",
  RIGHT = "right",
}

// 图例标记
export interface Legend {
  show?: boolean
  orient?: Legend_Orient
  left?: Legend_Left
}

// 图例标记 - 方向
export enum Legend_Orient {
  HORIZONTAL = "horizontal",
  VERTICAL = "vertical",
}

// 图例标记 - 位置
export enum Legend_Left {
  LEFT = "left",
  CENTER = "center",
  RIGHT = "right",
}

// 鼠标放上去图表后的提示
export interface Tooltip {
  trigger?: Tooltip_Trigger | string
  axisPointer?: {
    type?: Tooltip_AxisPointer_Type | string
  }
}

// 图表提示 - 类型
export enum Tooltip_Trigger {
  ITEM = "item",
  AXIS = "axis",
  NONE = "none",
}

// 图表提示 - 样式
export enum Tooltip_AxisPointer_Type {
  LINE = "line",
  SHADOW = "shadow",
  NONE = "none",
  CROSS = "cross",
}

// 通用数据配置项
export interface Series {
  // 通用
  id: number
  data?: any
  type?: string
  name?: string
  itemStyle?: ItemStyle
  // 柱状图
  barWidth?: number //柱状宽度
  showBackground?: boolean //显示背景
  label?: LabelStyle //显示数值
  // 折线图
  areaStyle?: AreaStyle //阴影区域
  smooth?: boolean //是否平滑曲线
  // 饼图
  radius: Array<string> //图表大小，值如 50%
  labelLine?: LabelLine //是否显示关联线
}

// 柱状图 - 数值显示
export interface LabelStyle {
  show?: boolean
  position?: LabelPosition
}

// 柱状图 - 数值显示 - 位置
export enum LabelPosition {
  // top / left / right / bottom / inside / insideLeft / insideRight / insideTop / insideBottom / insideTopLeft / insideBottomLeft / insideTopRight / insideBottomRight
  TOP = "top",
  LEFT = "left",
  RIGHT = "right",
  BOTTOM = "bottom",
  INSIDE = "inside",
}

// 柱状图 - 条形区域
export interface ItemStyle {
  color?: string
}

// 折线图 - 阴影区域
export interface AreaStyle {
  color?: string
  opacity?: number
}

export interface LabelLine {
  show?: boolean
}
