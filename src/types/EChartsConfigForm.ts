import { Title, Series, Tooltip, Legend } from "./EChartsCommonConfig"
// 图表配置项
export interface EChartsConfigForm {
  title?: Title
  tooltip?: Tooltip
  legend?: Legend
  series: Series[]
}
