import { reactive } from "vue"
import {
    Tooltip_Trigger,
    Tooltip_AxisPointer_Type,
} from "../types/EChartsCommonConfig"

// 图表类型
export enum ChartType {
  LINE = "line",
  BAR = "bar",
  PIE = "pie",
  RADAR = "radar",
}

/**
 * 各图表的初始化配置项
 * Tips: 注意！必须使用闭包，避免外部修改该配置项
 *       返回前，把 BASE 的配置项合并到各单独配置项中
 */
export function chartDefaultConfig() {
    const BASE = () => {
        return reactive({
            title: {
                text: "",
                subTitle: "",
                left: "center",
            },
            tooltip: {
                trigger: Tooltip_Trigger.AXIS,
                axisPointer: {
                    type: Tooltip_AxisPointer_Type.NONE,
                },
            },
            legend: {
                show: true,
                orient: "vertical",
                left: "right",
            },
            series: [],
        })
    }
    const LINE = () => {
        const line = reactive({
            xAxis: {
                type: "category",
                data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
            },
            yAxis: {
                type: "value",
            },
            series: [
                {
                    id: new Date().getTime(),
                    data: [
                        getRandom(),
                        getRandom(),
                        getRandom(),
                        getRandom(),
                        getRandom(),
                        getRandom(),
                        getRandom(),
                    ],
                    type: "line",
                    smooth: false,
                },
            ],
        })
        return Object.assign(BASE(), line)
    }
    const BAR = () => {
        const bar = reactive({
            xAxis: {
                type: "category",
                data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
            },
            yAxis: {
                type: "value",
            },
            series: [
                {
                    id: new Date().getTime(),
                    data: [
                        getRandom(),
                        getRandom(),
                        getRandom(),
                        getRandom(),
                        getRandom(),
                        getRandom(),
                        getRandom(),
                    ],
                    type: "bar",
                    showBackground: false,
                    itemStyle: {
                        color: "",
                    },
                    label: {
                        show: false,
                    },
                },
            ],
        })
        return Object.assign(BASE(), bar)
    }
    const PIE = () => {
        const pie = reactive({
            series: [
                {
                    id: new Date().getTime(),
                    name: "Pie Chart",
                    radius: ["50%", "40%"],
                    data: [
                        { value: 1048, name: "Search Engine" },
                        { value: 735, name: "Direct" },
                        { value: 580, name: "Email" },
                        { value: 484, name: "Union Ads" },
                        { value: 300, name: "Video Ads" },
                    ],
                    type: "pie",
                    itemStyle: {
                        color: "",
                    },
                    label: {
                        show: false,
                    },
                    labelLine: {
                        show: false,
                    },
                },
            ],
        })
        return Object.assign(BASE(), pie)
    }
    const RADAR = ()=>{
        const radar = reactive({
            radar: {
                indicator: [
                    { name: "Sales", max: 100 },
                    { name: "Administration", max: 100 },
                    { name: "Information Technology", max: 200 },
                    { name: "Customer Support", max: 100 },
                    { name: "Development", max: 300 },
                    { name: "Marketing", max: 100 }
                ]
            },
            series:[
                {
                    id: new Date().getTime(),
                    name: "Radar Chart",
                    type: "radar",
                    data: [
                        {
                            value: [getRandom(), getRandom(), getRandom(), getRandom(), getRandom(), getRandom()],
                            name: "Allocated Budget"
                        }
                    ]
                }
            ]
        })
        return Object.assign(BASE(), radar)
    }

    return {
        BASE,
        LINE,
        BAR,
        PIE,
        RADAR
    }
}

function getRandom() {
    return (Math.random() * 100).toFixed(1)
}
