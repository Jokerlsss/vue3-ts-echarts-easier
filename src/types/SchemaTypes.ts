// 用于生成配置项的输入框（该文件暂时弃用）

export enum SchemaTypes {
  STRING = "string",
  NUMBER = "number",
  SWITCH = "switch",
  COLOR = "color",
}

export const FiledPropsDefine = {
  value: {
    required: true,
  },
  label: {
    required: true,
    type: String,
  },
  type: {
    required: true,
    type: String,
    default: SchemaTypes.STRING,
  },
} as const
