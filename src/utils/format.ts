import XEUtils from 'xe-utils'
export const dateFormat = (value: any, fmt = 'yyyy/MM/dd HH:mm:ss') => {
  // 超出8小时的时间戳需减掉，才是正确的时间
  const OUT_HOUR_STAMP = 3600000 * 8
  const target = new Date(value).getTime() - OUT_HOUR_STAMP

  return XEUtils.toDateString(new Date(target), fmt)
}
