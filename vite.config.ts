import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import vueJsx from '@vitejs/plugin-vue-jsx'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vueJsx()],
  css: {
    preprocessorOptions: {
      less: {
        modifyVars: {
          hack: `true; @import (reference) "${path.resolve(
            'src/assets/global.less'
          )}";`,
        },
        javascriptEnabled: true,
      },
    },
  },
  // 启动应用后暴露ip
  server: {
    host: '0.0.0.0',
  },
})
